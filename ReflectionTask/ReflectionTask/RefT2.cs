﻿using System;
using System.Reflection;

namespace RefDemo
{
    public class MainClass
    {
        private static void Main()
        {

           


            //Step 1
            // Type T = Type.GetType("RefDemo.Customer");
            Type T = typeof(Customer);
            Console.WriteLine("Full Name = {0}", T.FullName);
            Console.WriteLine("Just the Name = {0}", T.Name);
            Console.WriteLine("Just the Namespace = {0}", T.Namespace);

            Console.WriteLine();





            //Step 2
            Console.WriteLine("Properties in Customer class");
            PropertyInfo[] properties = T.GetProperties();

            foreach (PropertyInfo Property in properties)
            {
                Console.WriteLine(Property.Name + "  " + Property.PropertyType.Name);
            }

            Console.WriteLine();





            //Step 3
            Console.WriteLine("Methods in Customer class");
            MethodInfo[] methods = T.GetMethods();

            foreach (MethodInfo method in methods)
            {
                Console.WriteLine(method.Name + "  " + method.ReturnType.Name);
            }




            //Step 4
            Console.WriteLine("Constructors in Customer class");
            ConstructorInfo[] constructors = T.GetConstructors();

            foreach (ConstructorInfo constructor in constructors)
            {
                Console.WriteLine(constructor.ToString());
            }
        }
    }



    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Customer(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }

        public Customer()
        {
            this.Id = -1;
            this.Name = string.Empty;
        }

        public void PrintId()
        {
            Console.WriteLine("ID = {0}", this.Id);
        }

        public void PrintName()
        {
            Console.WriteLine("Name = {0}", this.Name);
        }
    }
}
