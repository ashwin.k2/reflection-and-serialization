﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace BimarySerialization
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person() { FirstName = "John", LastName = "Doe" };
            string filePath = @"D:\\Source\\Desktop\\test.txt";
            DataSerializer dataSerializer = new DataSerializer();
            Person p = null;

            dataSerializer.BinarySerialize(person, filePath);
             
            p = dataSerializer.BinaryDeserialize(filePath) as Person;

            //dataSerializer.XmlSerialize(typeof(Person),person, filePath);

            //p = dataSerializer.XmlDeserialize(typeof(Person) ,filePath) as Person;

            //dataSerializer.JsonSerialize(person, filePath);

            //p = dataSerializer.JsonDeserialize(typeof(Person) ,filePath) as Person;

            Console.WriteLine(p.FirstName);
            Console.WriteLine(p.LastName);

            Console.ReadLine();
        }
    }
    [Serializable]
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    

    class DataSerializer
    {

        //Binary Serialization
        public void BinarySerialize(object data, string filepath)
        {
            FileStream fileStream;
            BinaryFormatter bf = new BinaryFormatter();
            if(File.Exists(filepath)) File.Delete(filepath);
            fileStream = File.Create(filepath);
            bf.Serialize(fileStream, data);
            fileStream.Close();
        }

        public object BinaryDeserialize (string filepath)
        {
            object obj = null;

            FileStream fileStream;
            BinaryFormatter bf =  new BinaryFormatter();
            if(File.Exists(filepath))
            {
                fileStream = File.OpenRead(filepath);
                obj = bf.Deserialize(fileStream);
                fileStream.Close();
            }

            return obj;

        }

        // XML Serialization 

        public void XmlSerialize (Type datatype, object data, string filepath)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(datatype);
            if (File.Exists(filepath)) File.Delete(filepath);
            TextWriter writer = new StreamWriter(filepath);
            xmlSerializer.Serialize(writer, data);
            writer.Close();
        }

        public object XmlDeserialize (Type datatype, string filepath)
        {
            object obj = null;

            XmlSerializer xmlSerializer = new XmlSerializer(datatype);
            if(File.Exists(filepath))
            {
                TextReader textReader = new StreamReader(filepath);
                obj = xmlSerializer.Deserialize(textReader);
                textReader.Close();
            }

            return (obj);
        }

        // Json Serialization
        public void JsonSerialize(object data, string  filepath)
        {
            JsonSerializer jsonSerializer = new JsonSerializer();
            if(File.Exists(filepath)) File.Delete (filepath);
            StreamWriter sw = new StreamWriter(filepath);
            JsonWriter jsonWriter = new JsonTextWriter(sw);
            jsonSerializer.Serialize(jsonWriter, data);
            jsonWriter.Close();
            sw.Close();

        }

        public object JsonDeserialize (Type datatype, string filepath)
        {
            JObject obj = null;
            JsonSerializer jsonSerializer = new JsonSerializer();
            if(File.Exists(filepath))
            {
                StreamReader sr = new StreamReader(filepath);
                JsonReader jsonReader = new JsonTextReader(sr); 
                obj = jsonSerializer.Deserialize(jsonReader) as JObject;
                jsonReader.Close();
                sr.Close();

              
            }

            return obj.ToObject(datatype);
        }
    }
}
